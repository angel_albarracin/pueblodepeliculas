namespace PuebloDePelícula.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actor",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(unicode: false),
                        Comercial_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pelicula", t => t.Comercial_ID)
                .Index(t => t.Comercial_ID);
            
            CreateTable(
                "dbo.Cine",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Ciudad = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Sala",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Cine_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Cine", t => t.Cine_ID)
                .Index(t => t.Cine_ID);
            
            CreateTable(
                "dbo.Funcion",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Horario = c.DateTime(nullable: false, precision: 0),
                        Pelicula_ID = c.Int(),
                        Sala_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pelicula", t => t.Pelicula_ID)
                .ForeignKey("dbo.Sala", t => t.Sala_ID)
                .Index(t => t.Pelicula_ID)
                .Index(t => t.Sala_ID);
            
            CreateTable(
                "dbo.Pelicula",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(unicode: false),
                        Duracion = c.Int(nullable: false),
                        Foto = c.String(unicode: false),
                        Sinopsis = c.String(unicode: false),
                        Tema = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sala", "Cine_ID", "dbo.Cine");
            DropForeignKey("dbo.Funcion", "Sala_ID", "dbo.Sala");
            DropForeignKey("dbo.Funcion", "Pelicula_ID", "dbo.Pelicula");
            DropForeignKey("dbo.Actor", "Comercial_ID", "dbo.Pelicula");
            DropIndex("dbo.Funcion", new[] { "Sala_ID" });
            DropIndex("dbo.Funcion", new[] { "Pelicula_ID" });
            DropIndex("dbo.Sala", new[] { "Cine_ID" });
            DropIndex("dbo.Actor", new[] { "Comercial_ID" });
            DropTable("dbo.Pelicula");
            DropTable("dbo.Funcion");
            DropTable("dbo.Sala");
            DropTable("dbo.Cine");
            DropTable("dbo.Actor");
        }
    }
}
