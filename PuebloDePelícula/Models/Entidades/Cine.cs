﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuebloDePelícula.Models.Entidades
{
    public class Cine
    {
        public int ID { get; set; }
        public string Ciudad { get; set; }
        public List<Sala> Salas { get; set; }
    }
}