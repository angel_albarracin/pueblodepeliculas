﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuebloDePelícula.Models.Entidades
{
    public class Actor
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
    }
}