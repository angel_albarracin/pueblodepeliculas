﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuebloDePelícula.Models.Entidades
{
    public abstract class Sala
    {
        public int ID { get; set; }
        public int Number { get; set; }
        public List<Funcion> Funciones { get; set; }
    }
}