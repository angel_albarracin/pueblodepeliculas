﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuebloDePelícula.Models.Entidades
{
    public class Funcion
    {
        public int ID { get; set; }
        public Pelicula Pelicula { get; set; }
        public DateTime Horario { get; set; }
    }
}