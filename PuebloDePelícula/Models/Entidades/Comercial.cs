﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuebloDePelícula.Models.Entidades
{
    public class Comercial : Pelicula
    {
        public List<Actor> Actores { get; set; }
    }
}