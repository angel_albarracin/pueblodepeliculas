﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuebloDePelícula.Models.Entidades
{
    public abstract class Pelicula
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public int Duracion { get; set; }
        public string Foto { get; set; }
        public string Sinopsis { get; set; }
    }
}