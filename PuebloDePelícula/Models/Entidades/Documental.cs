﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuebloDePelícula.Models.Entidades
{
    public enum Tema : int { Biográfico, VidaSalvaje, Ecología, Histórico, Sociales };

    public class Documental : Pelicula
    {
        public Tema Tema { get; set; }
    }
}