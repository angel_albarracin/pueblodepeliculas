﻿using MySql.Data.Entity;
using PuebloDePelícula.Models.Entidades;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PuebloDePelícula.DAL
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class PuebloDePeliculaContext : DbContext
    {

        public PuebloDePeliculaContext() : base("PuebloDePeliculaContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Actor> Actores { get; set; }
        public DbSet<Cine> Cines { get; set; }
        public DbSet<Comercial> Comerciales { get; set; }
        public DbSet<Documental> Documentales { get; set; }
        public DbSet<Funcion> Funciones { get; set; }
        public DbSet<HD> HDs { get; set; }
        public DbSet<Normal> Normales { get; set; }
        public DbSet<Pelicula> Peliculas { get; set; }
        public DbSet<Sala> Salas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}